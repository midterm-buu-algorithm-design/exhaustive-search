/**
 *	BruteForce or Exhoustive Search Algorithm java
 *	By Anurak Yutthanawa
 * */

import java.util.Scanner;

public class BruteForce{

	static Scanner stdin = new Scanner(System.in);

	public static void main(String[] args){
		String searchStr = new String(), pattern = new String();
		int res;
		System.out.print("Enter Search String: ");
		searchStr = stdin.nextLine();
		System.out.print("Enter Pattern String: ");
		pattern = stdin.nextLine();
		System.out.println(searchStr+":"+searchStr.length()+" "+ pattern+":"+pattern.length());
		res = bruteForce(searchStr, pattern, searchStr.length()- 1, pattern.length() - 1);
		if (res == -1){
			System.out.println("Search pattern is not available");
		}
		else {
			
			System.out.println("Search pattern available at the location " + res);
		}
	}

	private static int bruteForce(String search, String pattern, int slen,int plen){
   		int i, j, k;

    		for (i = 0; i <= slen - plen; i++) {
        		for (j = 0, k = i; (search.charAt(k) == pattern.charAt(j)) &&
                           	(j < plen);
             		++j, ++k)
            		;
        		if (j == plen)
            			return k + 1;
    		}
    		return -1;
	}
}
